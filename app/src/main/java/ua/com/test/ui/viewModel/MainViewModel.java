package ua.com.test.ui.viewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import ua.com.test.network.models.UsersModel;
import ua.com.test.repositories.UsersRepository;
import ua.com.test.repositories.api.Callback;

public class MainViewModel extends AndroidViewModel {
    private MutableLiveData<UsersModel> users;
    private UsersRepository repository = new UsersRepository();

    public MainViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<UsersModel> getUsers(){
        if (users == null){
            users = new MutableLiveData<>();
        }
        return users;
    }

    public void requestData() {
        repository.getUsers(s -> users.postValue(s));
    }
}
