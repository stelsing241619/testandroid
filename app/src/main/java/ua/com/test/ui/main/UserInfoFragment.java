package ua.com.test.ui.main;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import butterknife.BindView;
import butterknife.ButterKnife;
import ua.com.test.R;
import ua.com.test.adapters.RVAdapter;
import ua.com.test.dto.UserInfoDto;
import ua.com.test.network.models.UsersModel;

public class UserInfoFragment extends Fragment {

    @BindView(R.id.person_photo)
    ImageView personPhoto;
    @BindView(R.id.firstName)
    TextView firstName;
    @BindView(R.id.secondName)
    TextView secondName;
    @BindView(R.id.email)
    TextView email;
    @BindView(R.id.age)
    TextView age;
    @BindView(R.id.address)
    TextView address;

    public static UserInfoFragment newInstance() {
        return new UserInfoFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.user_info_fragment, container, false);
        ButterKnife.bind(this, view);

        Bundle arg = getArguments();
        if(arg!=null){
            UserInfoDto userInfo = (UserInfoDto) arg.getSerializable("USER_INFO");
            firstName.setText(userInfo.getName().getFirst());
            secondName.setText(userInfo.getName().getLast());
            email.setText(userInfo.getEmail());
            age.setText(userInfo.getDob().getAge().toString());
            address.setText(userInfo.getLocation().getCity());

            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getContext()).build();
            ImageLoader imageLoader = ImageLoader.getInstance(); // Get singleton instance
            imageLoader.init(config);
            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .build();
            imageLoader.displayImage(userInfo.getPicture().getMedium(), personPhoto, options);
        }

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

}
