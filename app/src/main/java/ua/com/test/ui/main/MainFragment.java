package ua.com.test.ui.main;

import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import ua.com.test.R;
import ua.com.test.adapters.RVAdapter;
import ua.com.test.dto.UserInfoDto;
import ua.com.test.network.models.Result;
import ua.com.test.network.models.UsersModel;
import ua.com.test.ui.viewModel.MainViewModel;

public class MainFragment extends Fragment {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.requestButton)
    Button requestButton;

    private OnFragmentDataListener mListener;
    private MainViewModel mViewModel;
    LiveData<UsersModel> users;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof OnFragmentDataListener){
            mListener = (OnFragmentDataListener) context;
        }
    }

    public MainFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        RVAdapter adapter = new RVAdapter(getContext(), position -> {
            if(users.getValue()!=null) {
                if(mListener!=null) {
                    Result result = users.getValue().getResults().get(position);
                    UserInfoDto dto = new UserInfoDto(result);
                    mListener.onClickItem(dto);
                }
            }
        });

        users = mViewModel.getUsers();

        users.observe(this,usersModel -> {
            Log.d("USERS", usersModel.toString());
            adapter.setUsersModel(usersModel);
            adapter.notifyDataSetChanged();
            recyclerView.setAdapter(adapter);
        });

        recyclerView.addOnItemTouchListener(new RecyclerView.SimpleOnItemTouchListener());

        requestButton.setOnClickListener(v -> mViewModel.requestData());
    }

    public interface OnFragmentDataListener{
        void onClickItem(UserInfoDto dto);
    }

}
