package ua.com.test.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import ua.com.test.network.models.Dob;
import ua.com.test.network.models.Id;
import ua.com.test.network.models.Location;
import ua.com.test.network.models.Login;
import ua.com.test.network.models.Name;
import ua.com.test.network.models.Picture;
import ua.com.test.network.models.Registered;
import ua.com.test.network.models.Result;

public class UserInfoDto implements Serializable {
    private String gender;
    private Name name;
    private Location location;
    private String email;
    private Login login;
    private Dob dob;
    private Registered registered;
    private String phone;
    private String cell;
    private Id id;
    private Picture picture;
    private String nat;

    public UserInfoDto(Result result) {
        gender = result.getGender();
        name = result.getName();
        location = result.getLocation();
        email = result.getEmail();
        login = result.getLogin();
        dob = result.getDob();
        registered = result.getRegistered();
        phone = result.getPhone();
        cell = result.getCell();
        id = result.getId();
        picture = result.getPicture();
        nat = result.getNat();
    }

    public String getGender() {
        return gender;
    }

    public Name getName() {
        return name;
    }

    public Location getLocation() {
        return location;
    }

    public String getEmail() {
        return email;
    }

    public Login getLogin() {
        return login;
    }

    public Dob getDob() {
        return dob;
    }

    public Registered getRegistered() {
        return registered;
    }

    public String getPhone() {
        return phone;
    }

    public String getCell() {
        return cell;
    }

    public Id getId() {
        return id;
    }

    public Picture getPicture() {
        return picture;
    }

    public String getNat() {
        return nat;
    }
}
