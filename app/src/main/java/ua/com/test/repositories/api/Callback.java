package ua.com.test.repositories.api;

public interface Callback<T> {
    void action(T s);
}
