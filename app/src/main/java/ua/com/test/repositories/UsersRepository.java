package ua.com.test.repositories;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import ua.com.test.network.RetrofitControl;
import ua.com.test.network.api.GetUsersApi;
import ua.com.test.network.models.UsersModel;
import ua.com.test.repositories.api.Callback;

public class UsersRepository {
    private UsersModel usersModel;

    public void getUsers(final Callback<UsersModel> callback){
        Retrofit retrofit = RetrofitControl.getRetrofit("https://randomuser.me");
        GetUsersApi getUsersApi = retrofit.create(GetUsersApi.class);

        Call<UsersModel> data = getUsersApi.getData(20);
        data.enqueue(new retrofit2.Callback<UsersModel>() {
            @Override
            public void onResponse(Call<UsersModel> call, Response<UsersModel> response) {
                if(response.isSuccessful()){
                    usersModel = response.body();
                    if(callback!=null){
                        callback.action(response.body());
                    }
                }
            }

            @Override
            public void onFailure(Call<UsersModel> call, Throwable t) {

            }
        });
    }

    public UsersModel getUsersModel(){
        return usersModel;
    }
}
