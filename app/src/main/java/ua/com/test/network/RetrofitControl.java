package ua.com.test.network;

import android.content.Context;

import androidx.annotation.CheckResult;
import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Retrofit control class
 *
 * @author  Yevhen Shcherbak
 * @version 1.0
 * @since   2017-05-07
 */
public class RetrofitControl {

    static private OkHttpClient.Builder httpClient = null;
    /**
     * Get retrofit object with a given address
     * @param address address to connect
     * @return retrofit object
     */
    @NonNull
    @CheckResult
    public static Retrofit getRetrofit( String address) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        return new Retrofit.Builder()
                .baseUrl(address)
                .client(getHttpClient().build())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
    }
    @CheckResult
    private static OkHttpClient.Builder getHttpClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        if(httpClient==null) {
            httpClient = new OkHttpClient.Builder();
            httpClient.readTimeout(5, TimeUnit.SECONDS);
            httpClient.connectTimeout(5, TimeUnit.SECONDS);
            httpClient.addInterceptor(logging);
            //httpClient.addInterceptor(new ChuckInterceptor(context));
        }
        return httpClient;
    }
}
