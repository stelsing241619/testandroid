package ua.com.test.network.api;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Query;
import ua.com.test.network.models.UsersModel;

public interface GetUsersApi {
  @GET("/api/")
  Call<UsersModel> getData(@Query("results") int number);
}
