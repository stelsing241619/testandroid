package ua.com.test;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import ua.com.test.dto.UserInfoDto;
import ua.com.test.ui.main.MainFragment;
import ua.com.test.ui.main.UserInfoFragment;

public class MainActivity extends AppCompatActivity implements MainFragment.OnFragmentDataListener {

    private String currentFragment;
    private MainFragment mainFragment;
    private UserInfoFragment userInfoFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        if (savedInstanceState == null) {
            mainFragment = MainFragment.newInstance();
            replaceFragment(mainFragment);
        }
    }

    @Override
    public void onClickItem(UserInfoDto dto) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("USER_INFO", dto);
        userInfoFragment = new UserInfoFragment();
        userInfoFragment.setArguments(bundle);
        replaceFragment(userInfoFragment);
    }

    @Override
    public void onBackPressed() {
        if(mainFragment!=null) {
            if (!replaceFragment(mainFragment)) {
                super.onBackPressed();
            }
        }else{
            mainFragment = MainFragment.newInstance();
            replaceFragment(mainFragment);
        }
    }


    private boolean replaceFragment (Fragment fragment){
        String backStateName = fragment.getClass().getName();

        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate (backStateName, 0);

        if (!fragmentPopped){ //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.container, fragment);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
        return fragmentPopped;
    }
}
