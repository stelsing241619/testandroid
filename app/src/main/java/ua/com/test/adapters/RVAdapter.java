package ua.com.test.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import ua.com.test.R;
import ua.com.test.network.models.Name;
import ua.com.test.network.models.UsersModel;

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.UsersViewHolder> {
    private UsersModel usersModel = new UsersModel();
    private Context context;
    private OnItemListener mOnItemListener;

    public RVAdapter(Context context, OnItemListener listener) {
        this.context = context;
        mOnItemListener = listener;
    }

    public void setUsersModel(UsersModel model){
        this.usersModel = model;
    }

    @NonNull
    @Override
    public UsersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_card, parent, false);
        return new UsersViewHolder(v, mOnItemListener);
    }

    @Override
    public void onBindViewHolder(@NonNull UsersViewHolder holder, int position) {
        Name name = usersModel.getResults().get(position).getName();
        holder.personName.setText(String.format("%s. %s %s", name.getTitle(), name.getFirst(), name.getLast()));

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context).build();
        ImageLoader imageLoader = ImageLoader.getInstance(); // Get singleton instance
        imageLoader.init(config);
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
        imageLoader.displayImage(usersModel.getResults().get(position).getPicture().getMedium(), holder.personPhoto, options);

        //holder.personPhoto.setImageResource(persons.get(i).photoId);
    }

    @Override
    public int getItemCount() {
        return usersModel.getResults().size();
    }

    public static class UsersViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CardView cv;
        TextView personName;
        ImageView personPhoto;
        OnItemListener onItemListener;

        UsersViewHolder(View itemView, OnItemListener onItemListener) {
            super(itemView);
            cv =itemView.findViewById(R.id.cv);
            personName = itemView.findViewById(R.id.person_name);
            personPhoto = itemView.findViewById(R.id.person_photo);
            this.onItemListener = onItemListener;

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onItemListener.onItemClick(getAdapterPosition());
        }
    }

    public interface OnItemListener{
        void onItemClick(int position);
    }
}
