package ua.com.test.repositories;

import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class UsersRepositoryTest {

    private CountDownLatch lock = new CountDownLatch(1);

    @Test
    public void getUsers() {
        UsersRepository usersRepository = new UsersRepository();
        usersRepository.getUsers(s -> {
            Assert.assertNotNull(s);
            Assert.assertEquals(s.getResults().size(),20);
        });

        try {
            lock.await(10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}