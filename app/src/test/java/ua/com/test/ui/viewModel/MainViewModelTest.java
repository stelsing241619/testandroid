package ua.com.test.ui.viewModel;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import ua.com.test.network.models.Result;
import ua.com.test.network.models.UsersModel;
import ua.com.test.repositories.UsersRepository;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MainViewModelTest {

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getUsers() {

        MainViewModel viewModel = mock(MainViewModel.class);
        UsersModel usersModel = mock(UsersModel.class);
        Result result = mock(Result.class);
        LiveData liveData = mock(LiveData.class);
        when(result.getEmail()).thenReturn("test@mail.com");
        when(result.getGender()).thenReturn("male");

        ArrayList<Result> list = mock(ArrayList.class);
        when(list.get(0)).thenReturn(result);

        when(usersModel.getResults()).thenReturn(list);
        when(viewModel.getUsers()).thenReturn(liveData);

        when(viewModel.getUsers().getValue()).thenReturn(usersModel);

        List<Result> results = viewModel.getUsers().getValue().getResults();
        assertEquals(results.get(0).getEmail(),"test@mail.com");
        assertEquals(results.get(0).getGender(),"male");
    }
}